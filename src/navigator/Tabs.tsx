import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import { Platform } from 'react-native';
// import { Tab2Screen } from '../screens/Tab2Screen';
import { Tab1Screen } from '../screens/Tab1Screen';
// import { Tab3Screen } from '../screens/Tab3Screen';
import { StackNavigator } from './StackNavigator';
import { colores } from '../theme/appTheme';
import { TopTabNavigator } from './TopTabNavigator';


export const Tabs = () => {
  return Platform.OS === 'ios' ? <TabsIOS />  : <TabsAndroid />;
};


const bottomTabAndroid = createMaterialBottomTabNavigator();

const TabsAndroid = () => {
  return (
    <bottomTabAndroid.Navigator
      sceneAnimationEnabled={true}
      barStyle={{
        backgroundColor: colores.primary,
      }}
      screenOptions={({route}) => (
        {
          tabBarIcon: () => {
            let iconName: string = '';
            switch (route.name) {
              case 'Tab1Screen':
                iconName = 'bandage-outline';
              break;
              case 'Tab2Screen':
                iconName = 'basketball';
              break;
              case 'StackNavigator':
                iconName = 'bookmarks';
              break;
            }
            return <Icon name={iconName} size={20} color="white" />;
          }
        }
      )}
    >
      <bottomTabAndroid.Screen name="Tab1Screen" options={{title: 'Tab1'}}component={Tab1Screen} />
      <bottomTabAndroid.Screen name="Tab2Screen" options={{title: 'Tab2'}} component={TopTabNavigator} />
      <bottomTabAndroid.Screen name="StackNavigator" options={{title: 'stack'}} component={StackNavigator} />
    </bottomTabAndroid.Navigator>
  );
}


const bottomTabIOS = createBottomTabNavigator();

const TabsIOS = () => {
  return (
    <bottomTabIOS.Navigator
      sceneContainerStyle= {{
        backgroundColor: 'white'
      }}
      screenOptions={({route}) => (
        {
          // options={{ headerShown: false }} -> en TabS.creen
          headerShown: false,
          tabBarActiveTintColor: colores.primary,
          tabBarStyle: {
            borderTopColor: colores.primary,
            borderTopWidth: 0,
            elevation: 0,
          },
          tabBarLabelStyle: {
            fontSize: 15,
          },
          tabBarIcon: () => {
            let iconName: string = '';
            switch (route.name) {
              case 'Tab1Screen':
                iconName = 'bandage-outline';
              break;
              case 'Tab2Screen':
                iconName = 'basketball';
              break;
              case 'StackNavigator':
                iconName = 'bookmarks';
              break;
            }
            return <Icon name={iconName} size={20} color="white" />;
          },
        }
      )}
    >
      {/* <Tab.Screen name="Tab1Screen" options={{title: 'Tab1', tabBarIcon: (props) => <Text style={{color: props.color}}>T1</Text>}} component={Tab1Screen} /> */}
      <bottomTabIOS.Screen name="Tab1Screen" options={{title: 'Tab1'}}component={Tab1Screen} />
      <bottomTabIOS.Screen name="Tab2Screen" options={{title: 'Tab2'}} component={TopTabNavigator} />
      <bottomTabIOS.Screen name="StackNavigator" options={{title: 'stack'}} component={StackNavigator} />
    </bottomTabIOS.Navigator>
  );
}