import React from 'react';
import { createDrawerNavigator, DrawerContentComponentProps, DrawerContentScrollView } from '@react-navigation/drawer';
// import { StackNavigator } from './StackNavigator';
import Icon from 'react-native-vector-icons/Ionicons';
import { SettingsScreen } from '../screens/SettingsScreen';
import { useWindowDimensions, Text, Image, View } from 'react-native';
import { styles } from '../theme/appTheme';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Tabs } from './Tabs';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';
const Drawer = createDrawerNavigator();

export const MenuLateral = () => {

  const { width } = useWindowDimensions();

  return (
    <Drawer.Navigator
    drawerContent={(props) => <MenuInterno {...props} />}
    screenOptions={{
        drawerType: width >= 768 ? 'permanent' : 'front',
        // drawerPosition: 'right',
        drawerStyle: { //Personaliza el menu lateral
          // backgroundColor: '#a9f004',
          width: 240,
        },
      }} >
      <Drawer.Screen name="Tabs" component={Tabs} />
      <Drawer.Screen name="SettingsScreen" component={SettingsScreen} />
    </Drawer.Navigator>
  );
};

const MenuInterno = ({ navigation }: DrawerContentComponentProps) => {

  const { authState: {username} } = useContext(AuthContext);

  return (
    <DrawerContentScrollView>
      {/* Parte del avatar */}
      <View style={styles.avatarContainer}>
        <Image
          source={{
            uri: 'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png',
          }}
          style={styles.avatar}
        />
      </View>

      {/* Opciones de menu */}
      <View style={styles.menuContainer}>

        <TouchableOpacity 
          style={{
            ...styles.menuBoton,
            flexDirection: 'row'
          }}
          onPress={() => navigation.navigate('Tabs')}
        >
          <Icon name="compass" size={20} color="black" />
          <Text style={styles.menuTexto}> Navegacion</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={{
            ...styles.menuBoton,
            flexDirection: 'row'
          }}
          onPress={() => navigation.navigate('SettingsScreen')}
        >
          <Icon name="cog" size={20} color="black" />
          <Text style={styles.menuTexto}> Ajustes</Text>

        </TouchableOpacity>

        {
          username && <Text> {username} </Text>
        }

      </View>

    </DrawerContentScrollView>
  );
}