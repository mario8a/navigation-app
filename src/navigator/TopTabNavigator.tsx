/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import { ChatScreen } from '../screens/ChatScreen';
import { ContactsScreen } from '../screens/ContactsScreen';
import { AlbumsScreen } from './../screens/AlbumsScreen';

import { LogBox } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { colores } from '../theme/appTheme';

LogBox.ignoreLogs(['Reanimated']);

const Tab = createMaterialTopTabNavigator();

export const TopTabNavigator = () => {

  const {top} = useSafeAreaInsets();

  return (
    <Tab.Navigator
      style={{paddingTop: top}}
      sceneContainerStyle={{
        backgroundColor: 'white',
      }}
      screenOptions={({route}) => (
        {
          tabBarPressColor: colores.primary, //Al darle click se ve el color
          tabBarShowIcon: true, // Visualiar los iconos
          tabBarIndicatorStyle: {
            backgroundColor: colores.primary, //Color de la rayina
          },
          tabBarStyle: { // Quitar la sombra en android
            borderTopColor: colores.primary,
            shadowColor: 'transparent',
            elevation: 0,
          },
          tabBarIcon: () => {
            let iconName: string = '';
            switch (route.name) {
              case 'Chat':
                iconName = 'bandage-outline';
              break;
              case 'Contacts':
                iconName = 'basketball';
              break;
              case 'Albums':
                iconName = 'bookmarks';
              break;
            }
            return <Icon name={iconName} size={20} color="black" />;
          },
        }
      )}
    >
      <Tab.Screen name="Chat" component={ChatScreen} />
      <Tab.Screen name="Contacts" component={ContactsScreen} />
      <Tab.Screen name="Albums" component={AlbumsScreen} />
    </Tab.Navigator>
  );
};
