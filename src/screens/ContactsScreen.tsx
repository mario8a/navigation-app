import React, { useContext } from 'react';
import { Button, View } from 'react-native';
import { Text } from 'react-native-paper';
import { styles } from '../theme/appTheme';
import { AuthContext } from '../context/AuthContext';

export const ContactsScreen = () => {

  const { signIn, logout ,authState: {isLoggedIn} } = useContext(AuthContext);

  return (
    <View style={styles.globalMargin}>
      <Text style={styles.title}>ContactsScreen</Text>

      {
        !isLoggedIn && <Button
                        title="Sign In"
                        onPress={signIn}
                      />
      }

      {
        isLoggedIn && <Button
                        title="Logout"
                        onPress={logout}
                      />
      }

    </View>
  );
};
