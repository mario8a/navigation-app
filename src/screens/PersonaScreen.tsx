/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { Text, View } from 'react-native';
import { styles } from '../theme/appTheme';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParams } from '../navigator/StackNavigator';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';


//Forma sucia para obtener los props <RouteList, pageName>
// RouteList -> Se definio en el stackNavgator
interface Props extends StackScreenProps<RootStackParams, 'PersonaScreen'>{};

// interface RouterParams {
//   id: number;
//   nombre: string;
// }

export const PersonaScreen = ({route, navigation}: Props) => {


  const { changeUsername } = useContext(AuthContext);

  //Poner los props como forma rapida;
  // const params = route.params as RouterParams;
  const params = route.params;

  useEffect(() => {
    navigation.setOptions({
      title: params.nombre,
    });
  }, []);

  useEffect(() => {
    changeUsername(params.nombre);
  }, []);


  return (
    <View style={styles.globalMargin}>
      <Text style={styles.title}>
        {
          JSON.stringify(params, null, 3)
        }
      </Text>
    </View>
  );
};
