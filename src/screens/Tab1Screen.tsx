import React, { useEffect } from 'react';
import { Text, View } from 'react-native';
import { styles } from '../theme/appTheme';
import { TouchableIcon } from '../components/TouchableIcon';


export const Tab1Screen = () => {


  useEffect(() => {

    console.log('Tab1Screen');
  }, []);

  return (
    <View style={styles.globalMargin}>
      <Text style={styles.title}>Iconos</Text>


      <TouchableIcon iconName="airplane" />
      <TouchableIcon iconName="alarm" />
      <TouchableIcon iconName="bandage" />
    </View>
  );
};
